package main

import (
	"bytes"
	"flag"
	"fmt"
	"math/rand"
)

const (
	appName    = "rgen"
	appVersion = "0.1"

	defaultLength = 32

	charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-*/{}[]()'~$!?.;:<>"
)

func main() {
	// Flags
	var (
		length int
	)

	flag.IntVar(&length, "length", defaultLength, "Length")

	flag.Parse()

	// Generate
	var r bytes.Buffer

	for i := 0; i < length; i++ {
		r.WriteRune(rune(charset[rand.Intn(len(charset)-1)]))
	}

	fmt.Print(r.String())
}
